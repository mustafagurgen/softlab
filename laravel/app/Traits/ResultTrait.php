<?php

namespace App\Traits;

use Illuminate\Http\Request;
use stdClass;

// use Validator;

trait ResultTrait
{

  
  
  public function result_fail($data) {
    return ['result'=> -1, "data" => $data];
  }

	public function result_ok($data)
  {
    return ['result'=>1, "data" => $data];
  }

	public function return_result($data)
  {
    return [$data];
  }

}
