<?php

namespace App\Traits;

use Illuminate\Http\Request;
use stdClass;

// use Validator;

trait PayloadTrait
{

	public function get_pr($neler)
  {
    $request = new Request();
    $pr = (object) $request->json()->get('pr');

    $return_value = new stdClass();
    foreach($neler as $v) {
			if(property_exists($pr,$v)) {
				$return_value->$v = !is_array($pr->$v) ? trim($pr->$v):$pr->$v; 
			}
    }
    return $return_value;
  }

  public function get_req($neler)
  {
    $pr =  Request()->all();
    
    $return_value = new stdClass();
    foreach($neler as $v) {
			if(array_key_exists($v,$pr)) {
				$return_value->$v = !is_array($pr[$v]) ? trim($pr[$v]):$pr[$v]; 
      }
    }
    return $return_value;
  }

  /*
  public function validate_pr($data,$rules=null){
    if (gettype($data) === 'object') { $data = get_object_vars($data); }
    if (!$rules) {
      if (property_exists($this,'rules')) {
        $rules = $this->rules;
      } else {
        return ['status'=>1];
      }
    }
    $validator = Validator::make($data, $rules);
    if ($validator->passes()) {
      return ['status'=>2];
    } else {
      return ['status'=>0,'errors'=>$validator->errors()->all(),'err'=>$validator->errors()];
    }
  }
  */

}
