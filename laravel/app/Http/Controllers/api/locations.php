<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Location;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

// use Illuminate\Http\Request;

class locations extends Controller
{
    use \App\Traits\PayloadTrait;
    use \App\Traits\ResultTrait;
    
    private function add ($pr) {
        $feed = new Location();
        $feed->country = $pr->country;
        $feed->city = $pr->city;
        $feed->longitude = $pr->long; 
        $feed->latitude = $pr->lat; 
        $feed->slug =  Str::slug($pr->city, '-'); 
        $feed->save();
        return  $this->result_ok($feed); 
    }

    private function update ($pr) {
        $feed = new Location();
        if ($pr->new <> 1) $feed = Location::select('*')->where('id',$pr->id)->first();
        $feed->country = $pr->country;
        $feed->city = $pr->city;
        $feed->longitude = $pr->long; 
        $feed->latitude = $pr->lat; 
        $feed->slug =  Str::slug($pr->city, '-'); 
        $feed->save();
        return  $this->result_ok($feed); 
    }

    public function create_location () {
        try {
            $pr = $this->get_pr(['id','country','city','long','lat','new']);
            if ($pr->id > 0) return $this->update($pr); 
            return  $this->add($pr);
        }catch (Exception $ex) {
            return  $this->result_fail($ex->getMessage()); 
        }
    }

    public function list_locations () {
        try {
            $cats = Location::select(DB::raw('id,country,city,longitude,latitude,slug'))->limit(100)->get();
            return $cats;
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }

    }

    public function get_location () {
        try {
            $pr = $this->get_pr(['slug']);
            $cat = Location::select('*')->where('slug',$pr->slug)->first();
            return $this->return_result($cat);
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }
    }
}
