<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Venue;
use App\Model\Category;
use App\Model\Location;
// use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Str;

class venues extends Controller
{
    use \App\Traits\PayloadTrait;
    use \App\Traits\ResultTrait;
    
    private function add ($pr) {
        $feed = new Venue();
        $feed->cat_id = $pr->cat_id;
        $feed->loc_id = $pr->loc_id;
        $feed->save();
        return  $this->result_ok($feed); 
    }

    private function update ($pr) {
        $feed = new Venue();
        if ($pr->new <> 1) $feed = Venue::select('*')->where('id',$pr->id)->first();
        $feed->cat_id = $pr->cat_id;
        $feed->loc_id = $pr->loc_id;
        $feed->save();
        return  $this->result_ok($feed); 
    }

    public function create_venue () {
        try {
            $pr = $this->get_pr(['id','cat_id','loc_id','new']);
            if ($pr->id > 0) return $this->update($pr); 
            return  $this->add($pr);
        }catch (Exception $ex) {
            return  $this->result_fail($ex->getMessage()); 
        }
    }

    public function list_venues () {
        try {
            $cats = Venue::select(DB::raw('*'))->limit(100)->get();
            return $cats;
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }

    }

    public function get_venue () {
        try {
            $pr = $this->get_pr(['slug']);
            $cat = Venue::select('*')->where('slug',$pr->slug)->first();
            return $this->return_result($cat);
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }
    }

    public function categories () {
        try {
            // $pr = $this->get_pr(['main','offset']);

            $cats = Category::select('*')->where('specs',1)->get();
            return $this->return_result($cats);
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }
    }

    public function explore () {
        try {
            $pr = $this->get_pr(['near','query']);

            $location = Location::select(DB::raw('id,country,city,slug,longitude,latitude'))->where('slug',$pr->near)->first();
            $cat = Category::select(DB::raw('id,main,title,slug'))->where('slug',$pr->query)->first();
            
            return $this->return_result(['loc'=>$location,'cat'=>$cat]);

            // return $this->return_result($cat);
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }
    }

}
