<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Category;
// use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class categories extends Controller
{
    use \App\Traits\PayloadTrait;
    use \App\Traits\ResultTrait;
    
    private function add ($pr) {
        $feed = new Category();
        $feed->main = (int) $pr->main;
        $feed->specs = (int) $pr->specs;
        $feed->title = $pr->title; 
        $feed->slug =  Str::slug($pr->title, '-'); 
        $feed->save();
        return  $this->result_ok($feed); 
    }

    private function update ($pr) {
        $feed = new Category();
        if ($pr->new <> 1) $feed = Category::select('*')->where('id',$pr->id)->first();
        $feed->main = (int) $pr->main;
        $feed->specs = (int) $pr->specs;
        $feed->title = $pr->title; 
        $feed->slug =  Str::slug($pr->title, '-'); 
        $feed->save();
        return  $this->result_ok($feed); 
    }

    public function create_cat () {
        try {
            $pr = $this->get_pr(['id','main','specs','title','new']);
            if ($pr->id > 0) return $this->update($pr); 
            return  $this->add($pr);
        }catch (Exception $ex) {
            return  $this->result_fail($ex->getMessage()); 
        }
    }

    public function list_cats () {
        try {
            // $pr = $this->get_pr(['specs','slug']);
            $cats = Category::select(DB::raw('id,title,slug,main'))->get();
            return $cats;
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }

    }

    public function get_cat () {
        try {
            $pr = $this->get_pr(['slug']);
            $cat = Category::select('*')->where('slug',$pr->slug)->first();
            return $this->return_result($cat);
        }
        catch (Exception $ex) {
            return $this->result_fail($ex->getMessage());
        }
    }

}
