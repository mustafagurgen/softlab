<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\File as HttpFile;
use Symfony\Component\HttpFoundation\File\File as FileFile;

class TraitMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:trait';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new trait';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
