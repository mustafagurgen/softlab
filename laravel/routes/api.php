<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('cats', 'v2\improvements@check911');
/*
Route::get('/cats', function () {
    return ['aa'=>'bb'];
});
/*
Route::post('/cats', function () {
    return ['aa'=>'bb'];
});
*/

## admin
Route::post('/cats/create','api\categories@create_cat');
Route::post('/cats/list','api\categories@list_cats');
Route::post('/cats/get','api\categories@get_cat');

Route::post('/locations/create','api\locations@create_location');
Route::post('/locations/list','api\locations@list_locations');
Route::post('/locations/get','api\locations@get_location');

// Route::post('/venues/create','api\venues@create_venue');
// Route::post('/venues/list','api\venues@list_venues');
// Route::post('/venues/get','api\venues@get_venue');

Route::post('/venues/categories','api\venues@categories');
Route::post('/venues/explore','api\venues@explore');

