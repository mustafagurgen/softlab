import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Venues from '../views/Venues.vue'
// import VenuesQuery from '../views/VenuesQuery.vue'


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/venues/explore',
    name: 'Venues',
    component: Venues
  },
  // {
  //   path: '/venues/explore/:near/:query',
  //   name: 'VenuesQuery',
  //   component: VenuesQuery
  // },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes,
  mode:"history"
})

export default router
